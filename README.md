# WHAT: 
Learning about Users allows us to build context for the problems we’re trying to solve through the Product. As part of the Plan/Analyze (Discover Phase) of UCD, the work we do during this phase will inform everything that follows in the later phases of the UCD framework. We use User Interviews to achieve this.  ​

User Interviews are a wide-spanning set of semi-structured interviews with anyone who has an interest in a project’s success, including users.

# WHY: 
User Interviews(and learning more about the Users) lay the foundation for the [Personas](https://www.nngroup.com/articles/persona/), which will become the common currency for discussing user needs throughout the project. This also lets the Team build consensus about the problem statement and research objectives. By using the insights we will also be able to create A/B tests, experiments, and better landing pages as well. 

# HOW:
The following are the steps involved in User Interviews -  

1. Preparation - [Issue](https://gitlab.com/smarter-codes/guidelines/product-design-group/learning-about-users/-/issues/1
)

2. Recruiting Participants - [Issue](https://gitlab.com/smarter-codes/guidelines/product-design-group/learning-about-users/-/issues/2
)

3. The Interview - [Issue](https://gitlab.com/smarter-codes/guidelines/product-design-group/learning-about-users/-/issues/3)

4. Synthesizing the Research - [Issue](https://gitlab.com/smarter-codes/guidelines/product-design-group/learning-about-users/-/issues/5
)

5. Brainstorm User Features - [Issue](https://gitlab.com/smarter-codes/guidelines/product-design-group/learning-about-users/-/issues/6
)

6. Retrospection - [Issue](https://gitlab.com/smarter-codes/guidelines/product-design-group/learning-about-users/-/issues/7
)


